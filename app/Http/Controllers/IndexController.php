<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class IndexController extends Controller
{
    private $token = 'QlnRWTTWw9lv3kjxy1A8byjUmBQedYqb';
    private $uri = 'https://superposuda.retailcrm.ru';
    private $create_order = '/api/v5/orders/create';
    private $get_items = '/api/v5/store/products';

    public function show() {
        //запрашиваю список товаров
        $request = Http::get($this->uri . $this-> get_items . '?apiKey=' . $this->token)['products'];

        //из ответа формирую массив с артикулом, брендом и наименованием
        $arr = $this->get_items($request);

        //формирую массив со списком брендов
        $brands = $this->get_brands($arr);

        return view('pages.index', [
            'items' => $arr,
            'brands' => $brands
        ]);
    }

    public function create_order (Request $request) {

        //формирую массив из данных формы и отправляю по Api
        $order = [
            'status' => 'trouble',
            'orderType' => 'fizik',
            'site' => 'test',
            'orderMethod' => 'test',
            'number' => '08041991',
            'name' => $request->name,
            'item' => [
                'article' => $request->article,
                'brand' => $request->brand,
                'title' => $request->item_name
            ]
            ];

        $http_req = Http::post($this->uri . $this->create_order, [
            'apiKey' => $this->token,
            'order' => json_encode($order)
        ]);

        //возвращаю ответ сервера
        return $http_req;
    }

    //функция для  получения основной информации о товаре
    private function get_items($arr) {
        $items = [];

        foreach ($arr as $item) {
            $elem = [
                "article" => $item['article'],
                "brand" => $item['manufacturer'],
                "name" => $item['name']
            ];

            array_push($items, $elem);
        }

        return $items;
    }

    //функция для получения массива брендов
    private function get_brands($arr) {
        $brands = [];

        foreach ($arr as $item) {
            $flag = false;
            foreach ($brands as $brand) {
                if ($brand == $item['brand']) {
                    $flag = true;
                }
            }

            if (!$flag) {
                array_push($brands, $item['brand']);
            }
        }

        return $brands;
    }
}
