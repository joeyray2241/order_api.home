let item_name = document.querySelector('#item_name');
let article = document.querySelector('#article');

item_name.value = article.children[0].innerHTML;

article.addEventListener('click', function () {
    let arr = article.children;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i].value == this.value) {
            item_name.value = article.children[i].innerHTML;
        }
    }
});
