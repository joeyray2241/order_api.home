@extends('layouts.main')

@section('content')
    <form method="post" action="{{route('create_order')}}">
        @csrf
        <p>ФИО: <input type="text" name="name"></p>
        <!--костыль, чтобы не вставлять ссылку на реп постоянно xD-->
        <p>Комментарий клиента: <input type="text" name="comment" value="https://gitlab.com/joeyray2241/order_api.home.git"></p>

        <!--Костыль 2: передаю в контроллер наименование продукта-->
        <p hidden><input type="text" name="item_name" id="item_name"></p>

        <p>Артикул товара:
            <select name="article" id="article">
                @foreach($items as $elem)
                    <option value="{{$elem['article']}}">{{$elem['name']}}</option>
                @endforeach
            </select>
        </p>
        <p>Бренд товара:
            <select name="brand">
            @foreach($brands as $brand)
                <option value="{{$brand}}">{{$brand}}</option>
            @endforeach
        </select></p>
        <p><input type="submit"></p>
    </form>
@endsection
